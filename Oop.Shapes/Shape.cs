﻿namespace Oop.Shapes
{
	public class Shape
	{
		/// <summary>
		/// Площадь фигуры
		/// </summary>
		public double Area { get; }

		/// <summary>
		/// Периметр
		/// </summary>
		public double Perimeter { get; }

		/// <summary>
		/// Количество вершин
		/// </summary>
		public int VertexCount { get; }

		public bool IsEqual(Shape shape) => false;
	}
}
