﻿using System;
using NUnit.Framework;
using Oop.Shapes.Factories;

namespace Oop.Shapes.Tests
{
	[TestFixture]
	public class RectangleTests
	{
		private ShapeFactory _shapeFactory;

		[OneTimeSetUp]
		public void FixtureSrtUp()
		{
			_shapeFactory = new ShapeFactory();
		}

		[TestCase(0, 0)]
		[TestCase(-1, -1)]
		[TestCase(int.MinValue, int.MinValue)]
		[TestCase(-1, 1)]
		[TestCase(1, -1)]
		[TestCase(1, 0)]
		[TestCase(0, 1)]
		public void Constructor_InvalidSide_ThrowsArgumentOutOfRangeException(int a, int b)
		{
			Assert.Throws<ArgumentOutOfRangeException>(() =>
			{
				Shape _ = _shapeFactory.CreateRectangle(a, b);
			});
		}

		[TestCase(1, 2, ExpectedResult = 2)]
		[TestCase(5, 10, ExpectedResult = 50)]
		[TestCase(12, 12, ExpectedResult = 144)]
		public decimal Area___ReturnsExpectedValue(int a, int b)
		{
			Shape rectangle = _shapeFactory.CreateRectangle(a, b);
			var area = rectangle.Area;

			return (decimal)Math.Round(area, 0);
		}

		[TestCase(4, 5, ExpectedResult = 18)]
		[TestCase(7, 11, ExpectedResult = 36)]
		[TestCase(20, 30, ExpectedResult = 100)]
		public decimal Perimeter___ReturnsExpectedValue(int a, int b)
		{
			Shape rectangle = _shapeFactory.CreateRectangle(a, b);
			var perimeter = rectangle.Perimeter;

			return (decimal)Math.Round(perimeter, 0);
		}

		[Test]
		public void VertexCount___Returns4()
		{
			Shape square = _shapeFactory.CreateSquare(1);
			var vertexCount = square.VertexCount;

			Assert.AreEqual(4, vertexCount);
		}

		[TestCase(4, 5, ExpectedResult = true)]
		[TestCase(5, 4, ExpectedResult = true)]
		[TestCase(3, 5, ExpectedResult = false)]
		[TestCase(5, 3, ExpectedResult = false)]
		[TestCase(4, 3, ExpectedResult = false)]
		[TestCase(3, 4, ExpectedResult = false)]
		[TestCase(1, 2, ExpectedResult = false)]
		[TestCase(2, 1, ExpectedResult = false)]
		public bool IsEqual_OtherRectangle_ReturnsExpectedResult(int a, int b)
		{
			var rectangle1 = _shapeFactory.CreateRectangle(4, 5);
			var rectangle2 = _shapeFactory.CreateRectangle(a, b);

			return rectangle1.IsEqual(rectangle2);
		}

		[Test]
		public void IsEqual_Circle_ReturnsFalse()
		{
			var rectangle = _shapeFactory.CreateRectangle(4, 5);
			var circle = _shapeFactory.CreateCircle(4);

			var isEqual = rectangle.IsEqual(circle);

			Assert.False(isEqual);
		}

		[Test]
		public void IsEqual_Square_ReturnsFalse()
		{
			var rectangle = _shapeFactory.CreateRectangle(4, 5);
			var square = _shapeFactory.CreateSquare(4);

			var isEqual = rectangle.IsEqual(square);

			Assert.False(isEqual);
		}

		[Test]
		public void IsEqual_Triangle_ReturnsFalse()
		{
			var rectangle = _shapeFactory.CreateRectangle(4, 5);
			var triangle = _shapeFactory.CreateTriangle(4, 5, 6);

			var isEqual = rectangle.IsEqual(triangle);

			Assert.False(isEqual);
		}
	}
}